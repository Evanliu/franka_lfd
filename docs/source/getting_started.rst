Getting Started
========

Please follow the steps to test the package:

* Switch ON the franka control box and wait till the robot_status is yellow.
* Release the brakes by logging into the `robot <https://robot.franka.de/>`_.

.. note:: type the command ``source to catkin_ws/devel/setup.bash``, if required. 

* For recording the joint angles the model_example_controller is used. launch the following
    .. code-block:: shell
    
       roslaunch franka_example_controllers model_example_controller.launch robot_ip:=130.230.37.113 load_gripper:=true
  
  this will record the joint angles and create a txt file to be read by panda_python.
  
  .. note:: To be developed: Utlilise "Moveit" to record joint angles and create a GUI to record joint angles according to user input
  
* Open four terminals and type the following in each of them:

  
  .. code-block:: shell
  
     # terminal 1: roslaunch franka_control franka_control.launch robot_ip:=130.230.37.113 arm_id:=panda  load_gripper:=true
     # terminal 2: roslaunch franka_gripper franka_gripper.launch robot_ip:=130.230.37.113
     # terminal 3: roslaunch panda_moveit_config panda_moveit.launch controller:=position
     # terminal 4: roslaunch panda_moveit_config moveit_rviz.launch

* Run the node my_panda_moveit.py


.. note:: Link to videos:

    * `Franka-learning from demonstration (V1) <https://www.youtube.com/playlist?list=PLUgit2Zvqw2PP8rFsyGDYJiDteISTmHS2>`_.
